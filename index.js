const emojiRegex = require('emoji-regex');
const fs = require('fs')

let result = [];

process.stdin.resume();
process.stdin.setEncoding('utf8');

process.stdin.on('data', function (text) {
  console.log('received data:', text);
  parsePomodori(text);

  if (text === 'quit\n') {
    done();
  }
});

function parsePomodori(text) {
  const regDate = /\d{4}-\d{2}-\d{2}/g;
  const dates = text.match(regDate); 
  if (dates == null) return;

  const contents = getContents(text);
  const nodes = getNodes(dates, contents);

  console.log(nodes);
  if (nodes.length > 0) result.push(nodes);
}

function getContents(text) {
  const regDate = /\d{4}-\d{2}-\d{2}/g;
  var contents = text.split(regDate).map(c => {
    const regex = emojiRegex();
    let match;
    let pomodoriCount = 0;
    let canceledCount = 0;
    let firstMatchingIndex = -1;
    let completed = c.includes("[x]");
    let estimation = 0;

    if(match = c.match(/\((\d+)\)/))
      estimation = match[1];

    while(match = regex.exec(c)) {
      // console.log('match', match);
      if(firstMatchingIndex == -1) firstMatchingIndex = match.index;
      if(match[0] == `🍅` ) {
        pomodoriCount ++;
      } else if (match[0] == `❌` ) {
        canceledCount ++;
      }
    }
    console.log('pomorodi', pomodoriCount);
    const text = firstMatchingIndex > -1 && c.substr(0, firstMatchingIndex) || c;
    if (pomodoriCount > 0) return {
      text,
      completed,
      estimation,
      pomodoriCount,
      canceledCount};

    return null;
  });
  return contents.filter(x => !!x);
}

function getNodes(dates, contents) {
  var nodes = dates.map((d, i) => ( Object.assign({}, contents[i], {date: d})));
  if (nodes.length > 1)
  {
    const text = nodes[0].text;
    const completed = nodes[0].completed;
    const estimation = nodes[0].estimation;
    for(var i = 1; i < nodes.length; ++i )
    {
      nodes[i] = Object.assign({}, nodes[i], {text, completed, estimation});
    }
  }
  return nodes.filter(x => x.text);
}

function showStat(data)
{
  let dateObj = {};
  data.forEach(row => {
    row.forEach(item => {
      if (!dateObj[item.date]) dateObj[item.date] = 0;
      dateObj[item.date] += item.pomodoriCount;
    });
  });
  const totalCount = Object.values(dateObj).reduce((total, p) => total + p, 0);
  console.log('totalCount:', totalCount);
  const totalDate = Object.keys(dateObj).length;
  console.log('totalDate:', totalDate);

  const avgPerDate = totalCount / totalDate;
  const dateArr = Object.keys(dateObj).sort();
  const startDate = dateArr[0];
  const endDate = dateArr[dateArr.length-1];
  console.log(startDate, ' ~ ', endDate);
  console.log('avgPerDate: ', avgPerDate);

}

function done() {
  console.log('Now that process.stdin is paused, there is nothing more to do.');
  console.log('---------------- result -----------------');
  console.log(result);
  showStat(result);
  process.exit();
}
